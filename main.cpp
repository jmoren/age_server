#pragma comment (lib, "Ws2_32.lib")

#include <iostream>
#include <queue>

#include "./logger/logger.h"
#include "./parser/parser.h"
#include "./game/Game.h"
#include "./network/Connection.h"
#include "./network/myThread.h"

#define ERROR_PARSING -5;

using namespace std;

HANDLE pEventsMutex, sEventsMutex, logMutex;
Logger *logger = Logger::Instance();

queue<Message> qEvents;
queue<GameUpdate> sEvents;
int globalID = 0;

// shared operations between threads - All uses mutex
bool writeLog(string message){

	DWORD dwCount = 0, dwWaitResult;
	dwWaitResult = WaitForSingleObject(logMutex, INFINITE);
	
	if (dwWaitResult == WAIT_OBJECT_0){
		logger->log(Logger::TypeMessage::LOG_INFO, message);
		ReleaseMutex(logMutex);
	}
	else{
		return false;
	}

	return true;
};

// Queue managment
void getToProcess(int size, queue<Message> *pList){
	DWORD dwCount = 0, dwWaitResult;
	dwWaitResult = WaitForSingleObject(pEventsMutex, INFINITE);

	if (dwWaitResult == WAIT_OBJECT_0){
		for (int i = 0; i < size; i++){
			Message m = qEvents.front();
			(*pList).push(m);
			qEvents.pop();
		}
		
		ReleaseMutex(pEventsMutex);
	}
};

GameUpdate getToSend(){
	DWORD dwCount = 0, dwWaitResult;
	dwWaitResult = WaitForSingleObject(sEventsMutex, INFINITE);

	// initialize this message to check if valid to send
	GameUpdate message;
	message.total = 0;

	if (dwWaitResult == WAIT_OBJECT_0){
		if (!sEvents.empty()){
			message = sEvents.front();
			sEvents.pop();
		}

		ReleaseMutex(sEventsMutex);
	}

	return message;

};

bool addToSend(GameUpdate message){
	DWORD dwCount = 0, dwWaitResult;
	dwWaitResult = WaitForSingleObject(sEventsMutex, INFINITE);

	if (dwWaitResult == WAIT_OBJECT_0){
		sEvents.push(message);
		ReleaseMutex(sEventsMutex);
	}
	else{
		return false;
	}

	return true;
};

bool addToQueue(Message message){
	DWORD dwCount = 0, dwWaitResult;
	dwWaitResult = WaitForSingleObject(pEventsMutex, INFINITE);

	if(dwWaitResult == WAIT_OBJECT_0){
		qEvents.push(message);
		ReleaseMutex(pEventsMutex);
	}
	else{
		return false;
	}

	return true;
};

// Threads
DWORD WINAPI senderThreadHandler(LPVOID argument){
	writeLog("Started sender thread");
	ServerConnection *server = (ServerConnection*)argument;
	
	while (1){
		if (!sEvents.empty()){
			GameUpdate gameMessage = getToSend();
			if (gameMessage.total > 0){
				server->notifyAll(gameMessage);
			}
		}
		
		Sleep(500);
	}

	return 0;
}

DWORD WINAPI clientThreadHandler(LPVOID argument){
	ClientConnection *client = (ClientConnection*)argument;
	Game *game = client->game;

	int result;
	int res;
	char buffer[sizeof(Message)] = { 0 };
	char logmsg[100] = { 0 };
	Message message;
	Message response;

	sprintf_s(logmsg, "Inicia el thread de un cliente socket_id: %d", client->socket);
	writeLog(logmsg);
	Player *p;

	while (1){

		result = client->socket->recieveMessage(buffer);
		if (result == -1){
			writeLog("Client connection is lost");
			message.type = 4;
			strncpy_s(message.name, client->name.c_str(), sizeof(message.name));
			addToQueue(message);
			return -90;
		}
		if (result > 0){
			message.deserialize(buffer);

			if (message.type == 1){
				client->startLobby(buffer);
				if (client->connected && !client->in_lobby){
					client->sendFullData();
				}
			}
			else if (message.type == 2){
				p = game->players[string(message.name)];
				p->connected = true;
				
				writeLog("-- Reconnect player");
				client->connected = PLAYER_STATUS::CONNECTED;
				GameStatus gs = game->getCurrentStatus(message.name);
				strncpy_s(gs.current_name, message.name, sizeof(gs.current_name));

				writeLog("-- Send current game status");
				client->sendElements(gs);
				client->sendPlayers(gs);
				client->sendRoad(gs);

			} else if(message.type == 6){
				p = game->players[string(message.name)];
				if(p)
					p->wait = 0;
			}
			else{
				addToQueue(message);
			}

			memset(buffer, 0, sizeof(buffer));

		}

	}

	return 0;
}

DWORD WINAPI serverThreadHandler(LPVOID argument){
	writeLog("Started server thread");

	ServerConnection *server = (ServerConnection*)argument;
	Game *game = server->game;

	int status;
	int totalClients = 0;
	int gloablId = 0;

	// bind and listen to accept clients
	status = server->socket->bindSocket();
	if (status == -1){
		writeLog("Error binding socket");
		delete(server->socket);
		WSACleanup();
		return -1;
	};

	status = server->socket->listenForClients();
	if(status == -1){
		writeLog("Error listening socket");
		delete(server->socket);
		WSACleanup();
		return -1;
	};

	myThread * clientHandle[MAX_NUM_CLIENTS];
	ClientConnection *clients[MAX_NUM_CLIENTS];

	for (int i = 0; i < MAX_NUM_CLIENTS; i++){
		clients[i] = NULL;
	}


	writeLog("Start listening for new clients");

	// start accepting clients
	while (1){
		
		TcpSocket* client;
		string clientName;
		client = server->socket->acceptClient(clientName);


		if (client && totalClients < MAX_NUM_CLIENTS - 1){
			writeLog("Accepted new client");

			clients[totalClients] = new ClientConnection(client, game);
			server->addConnection(clients[totalClients]);

			clientHandle[totalClients] = new myThread(clientThreadHandler, (void*)clients[totalClients]);
			clientHandle[totalClients]->execute();

			totalClients++;
		}
	}

	return 0;
}

// Game
int main(){
	Parser *parser;
	Config *config;
	Game *game;
	ServerConnection *serverConnection;
	queue<Message> pList;

	bool quit = false;
	int status;

	logger = Logger::Instance();
	logger->log(Logger::TypeMessage::LOG_INFO, "Welcome to age of empires - Multiplayer");
	logger->log(Logger::TypeMessage::LOG_INFO, "Start winsock");

	// initalize winsock library
	TcpSocket::initialize();

	// initialize game info
	logger->log(Logger::TypeMessage::LOG_INFO, "Start parsing game");
	config = new Config();
	parser = new Parser(config);
	status = parser->parse();
	
	if (status == -1){
		logger->log(Logger::LOG_ERROR, "Error parsing yaml file. Can not init");
		return ERROR_PARSING;
	}
	else{
		game = new Game(config);
	}


	// Create all mutex
	logger->log(Logger::TypeMessage::LOG_INFO, "Initialize connections");
	pEventsMutex = CreateMutex(NULL, FALSE, NULL);
	sEventsMutex = CreateMutex(NULL, FALSE, NULL);
	logMutex = CreateMutex(NULL, FALSE, NULL);

	if (logMutex == NULL || pEventsMutex == NULL || sEventsMutex == NULL){
		logger->log(Logger::TypeMessage::LOG_ERROR, GetLastError(), "CreateMutex error: %d");
		WSACleanup();
		return -1;
	}

	// initialize the server
	TcpSocket *server = new TcpSocket(config->port);
	if (server->mySocket == INVALID_SOCKET){
		WSACleanup();
		return SOCKET_ERROR;
	}
	server->name = "localhost";
	
	// start thread for server listen and accept clients
	serverConnection = new ServerConnection(server, game);
	myThread* serverThread = new myThread(serverThreadHandler, (void*)serverConnection);
	serverThread->execute();
	// create thread to send data
	myThread* serverSendThread = new myThread(senderThreadHandler, (void*)serverConnection);
	serverSendThread->execute();

	// initialize vars to process events
	int eventsToRead;
	bool processing = false;

	char logBuffer[255] = { 0 };
	GameUpdate message;
	int keepAliveResult;
	map<string, Player*>::iterator p;
	while (1){
		if (game->started){
			game->incrementWaitForAll();
			for (p = game->players.begin(); p != game->players.end(); p++) {
				Player *aPlayer = p->second;
				if (aPlayer && aPlayer->wait == 150 && aPlayer->connected){
					aPlayer->connected = PLAYER_STATUS::DISCONNECTED;
					aPlayer->hasUpdates = true;
				}
			}

			if (qEvents.size() > 0 && !processing){
				processing = true;

				if ((eventsToRead = qEvents.size()) > 5)
					eventsToRead = 5;

				getToProcess(eventsToRead, &pList);
				game->processEvents(&pList);

				processing = false;
			}

			message = game->getUpdates();

			if (game->hasUpdatesToSend)
				addToSend(message);

		}
		else{
			game->started = game->checkLobby();
			if (game->started){
				serverConnection->startAllPlayers();;
			}
		}
		/*** wait tick or some prudent time ***/
		Sleep(60);
	}
	
	if (serverConnection)
		delete serverConnection;
	
	if (game)
		delete game;

	CloseHandle(pEventsMutex);
	CloseHandle(sEventsMutex);
	CloseHandle(logMutex);

	WSACleanup();
	return 0;
}