#include "Position.h"


Position::Position(int anX, int anY)
{
	posx = anX;
	posy = anY;
}

void Position::setX(int anX){
	posx = anX;
}

void Position::setY(int anY){
	posy = anY;
}

int Position::getX(){
	return posx;
}

int Position::getY(){
	return posy;
}


Position::~Position(void)
{
}
