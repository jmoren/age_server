#pragma once
#include <vector>
#include "../game/Tile.h"
#include "Cell.h"

class AStar
{
public:
	AStar(int aDestX, int aDestY, int originX, int originY, int mapAncho, int mapAlto, vector<Tile*> tiles);
	vector<Position*> calculateRoad();
	vector<Cell*> getAdjacentCells(Cell *nextCell);
	Cell *getEntityOn(int x, int y);
	vector<Position*> getRoad();
	vector<Cell*> orderByF(vector<Cell*> openCells);
	void initialize(vector<Tile*> someTiles);
	~AStar(void);

private:
	bool contains(vector<Cell*> cells, Cell *cell);

	int originX;
	int originY;
	int destX;
	int destY;
	std::vector<Cell*> openCells;
	std::vector<Cell*> closeCells;
	Cell *lastCell;
	int ancho;
	int alto;
	vector<Cell*> tiles;
};

