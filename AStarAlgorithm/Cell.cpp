#include "Cell.h"


Cell::Cell(Tile* tile){
	x = tile->posx;
	y = tile->posy;
	position = new Position(x, y);
	previousCell = nullptr;
	isWalkable = tile->is_walkable();
	G = 0;
	F = 0;
	H = 0;
}

void Cell::setPreviousCell(Cell *aCell){
	previousCell = aCell;
}

Position *Cell::getPosition(){
	return position;
}

Cell::~Cell(void){
	if(position)
		delete position;
}
