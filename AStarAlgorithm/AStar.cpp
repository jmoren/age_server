#include "AStar.h"

AStar::AStar(int aDestX, int aDestY, int anOriginX, int anOriginY, int mapAncho, int mapAlto, vector<Tile*> tiles)
{
	originX = anOriginX;
	originY = anOriginY;
	destX = aDestX;
	destY = aDestY;
	ancho = mapAncho;
	alto = mapAlto;
	this->initialize(tiles);
}

void AStar::initialize(vector<Tile*> someTiles){
	Tile *tile;
	for(size_t i=0; i < someTiles.size();i++){
		tile = someTiles.at(i);
		tiles.push_back(new Cell(tile));
	}
}

vector<Position*> AStar::calculateRoad(){

	openCells.push_back(tiles.at(originY * alto + originX));
	bool foundRoad = false;
	vector<Cell*> adjacentCells;

	while(!foundRoad && openCells.size() > 0){
		//Paso 1) Tomo el primer elemento de la lista abierta,lo saco y lo agrego a la lista cerrada.
		Cell *nextCell = openCells.at(openCells.size() - 1);
		openCells.pop_back();
		closeCells.push_back(nextCell);

		//Paso 2) Busco las celdas adyacentes a la celda extra�da.
		adjacentCells = this->getAdjacentCells(nextCell);

		if(nextCell->getX() == destX && nextCell->getY() == destY){
			lastCell = nextCell;
		}else{

		for(size_t i = 0; i < adjacentCells.size() ; i++){
            Cell *cell=adjacentCells.at(i);

			//Si la celda es la celda destino, termin�.
			//Si la celda representa algo que el personaje no puede atravesar la ignoro.
			//Si la celda ya est� en la lista cerrada, tambi�n la ignoro. 
			if(cell->getX() == destX && cell->getY() == destY && (cell->isWalkable) && !(this->contains(closeCells, cell)))
            {
                foundRoad = true;
				cell->setPreviousCell(nextCell);
				lastCell = cell;
				break;		
			}

			Cell *cellOnStudy = this->tiles.at(destY * this->alto + destX);

			if(!(cellOnStudy->isWalkable) && (this->contains(this->getAdjacentCells(cellOnStudy), cell))){
                foundRoad = true;
				lastCell = cell;
				break;						
			}


			//Si la celda ya est� en la lista abierta, compruebo si su nueva G es mejor que la actual
			if(this->contains(openCells, cell)){
				int newG;
					
				if(cell->getX() != nextCell->getX() && cell->getY() != nextCell->getY())
					newG = nextCell->getG() + 14;
				else
					newG = nextCell->getG() + 10;

				//Si el nuevo G es menor, recalculo los demas factores y pongo como padre de la celda a la celda extra�da. 
					if(cell->getG() != 0 && cell->getG()<newG){

						//En caso de que no sea mejor, la ignoro.
						continue;
					}
				};
				if(!(this->contains(closeCells, cell)))
					cell->setPreviousCell(nextCell);
					
					//Si los x e y son distintos se estar�a moviendo en diagonal y esto tiene un costo mayor
				if(cell->getX() != nextCell->getX() && cell->getY() != nextCell->getY())
					cell->setG(nextCell->getG() + 14);
				else
					cell->setG(nextCell->getG() + 10);
				int dx = abs(destX - cell->getX());
				int dy = abs(destY - cell->getY());
				cell->setH((dx+dy)*10);
				cell->setF(cell->getG() + cell->getH());

				if(cell->isWalkable && !(this->contains(closeCells, cell))){
					openCells.push_back(cell);
				}
			}
			openCells = this->orderByF(openCells);
		}
	}

	return this->getRoad();
}

vector<Cell*> AStar::orderByF(vector<Cell*> openCells){

	Cell *aux;

	for(size_t i = 0 ;i < openCells.size();i++){ 
		for (size_t j = 0; j <= i; j++){
			if(openCells.at(i)->getF() > openCells.at(j)->getF()){ 
				aux = openCells.at(i); 
				openCells[i] = openCells.at(j); 
				openCells[j] = aux; 
			} 
		} 
	} 
	return openCells;
}

bool AStar::contains(vector<Cell*> cells, Cell *cell){
	bool hasEntity = false;

	for (size_t j = 0; j < cells.size(); j++)
	{
		Cell *elementCell = cells.at(j);

		if(cell->getX() == elementCell->getX() && cell->getY() == elementCell->getY())
		{
			hasEntity = true;
		}
	}
	return hasEntity;
}

vector<Cell*> AStar::getAdjacentCells(Cell *nextCell){

	vector<Cell*> vectorToReturn;
	Cell *cell;

	cell = this->getEntityOn(nextCell->getX()- 1, nextCell->getY());
	if(cell && cell->isWalkable)
		vectorToReturn.push_back(cell);

	cell = this->getEntityOn(nextCell->getX() - 1, nextCell->getY() + 1);
	if(cell && cell->isWalkable)
		vectorToReturn.push_back(cell);

	cell = this->getEntityOn(nextCell->getX(), nextCell->getY() + 1);
	if(cell && cell->isWalkable)
		vectorToReturn.push_back(cell);

	cell = this->getEntityOn(nextCell->getX() + 1, nextCell->getY() + 1);
	if(cell && cell->isWalkable)
		vectorToReturn.push_back(cell);

	cell = this->getEntityOn(nextCell->getX() + 1, nextCell->getY());
	if(cell && cell->isWalkable)
		vectorToReturn.push_back(cell);

	cell = this->getEntityOn(nextCell->getX() + 1, nextCell->getY() - 1);
	if(cell && cell->isWalkable)
		vectorToReturn.push_back(cell);

	cell = this->getEntityOn(nextCell->getX(), nextCell->getY() - 1);
	if(cell && cell->isWalkable)
		vectorToReturn.push_back(cell);

	cell = this->getEntityOn(nextCell->getX() - 1, nextCell->getY() - 1);
	if(cell && cell->isWalkable)
		vectorToReturn.push_back(cell);

	return vectorToReturn;
}

Cell *AStar::getEntityOn(int anX, int anY){
	if(alto > anY && anY >= 0 && ancho > anX && anX >= 0)
		return tiles.at(anY * alto + anX);
	else
		return nullptr;
}

vector<Position*> AStar::getRoad(){
	vector<Position*> positions;
	Cell *previous;
	Cell *auxprevious;

	positions.push_back(lastCell->getPosition());
	previous = lastCell->getPreviousCell();
	lastCell->setPreviousCell(nullptr);

	while(previous != nullptr){
		positions.push_back(previous->getPosition());
		auxprevious = previous->getPreviousCell();
		//lo que sigue hacerlo tambien cuando se saque el print y se agregue la caminata
		previous->setPreviousCell(nullptr);
		previous = auxprevious;
	}

	if(positions.size() >1)
		positions.pop_back();
	
	return positions;
}

AStar::~AStar(void){
	for (size_t i = 0; i < tiles.size(); i++){
		delete tiles.at(i);
	}

}
