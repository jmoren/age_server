#pragma once

#include "../game/Tile.h"
#include "../AStarAlgorithm/Position.h"

class Cell
{
public:
	Cell(Tile *tile);

	int getX(){
		return x;
	};
	int getY(){
		return y;
	}

	int getG(){
		return G;
	}

	int getH(){
		return H;
	}

	int getF(){
		return F;
	}

	void setG(int anInteger){
		G = anInteger;
	}

	void setH(int anInteger){
		H = anInteger;
	}

	void setF(int anInteger){
		F = anInteger;
	}

	Cell *getPreviousCell(){
		return previousCell;
	}
	void setPreviousCell(Cell *nextCell);
	~Cell(void);

	bool isWalkable;
	Position *getPosition();

private:
	int x;
	int y;
	Cell *previousCell;
	int G;
	int F;
	int H;
	Position *position;
};

