#ifndef   _PARSER_H_
#define   _PARSER_H_

#include "yaml-cpp/yaml.h"
#include "../logger/logger.h"
#include "./config.h"

class Parser{
	public:
		Parser(Config *config);
		virtual ~Parser();
		Logger *logger;
		Config *config;
		YAML::Node nodeConfig;

		int parse();

	private:
		int openFiles();
		int parseElements();
		int parseConfiguration();

};

#endif