#ifndef   _CONFIG_H_
#define   _CONFIG_H_

#include <string>
#include <vector>
#include "../game/Tile.h"

#define FILE_NOT_FOUND			   (-1)
#define FILE_ERROR				   (-2)
#define ERROR_PARSER_KEY_NOT_FOUND (-3)

using namespace std;

struct Configuration{
	int height;
	int width;
	int velocity;
	int scroll; /*hace falta el scroll???*/
	int sizex;
	int sizey;
	int scrollVelocity; /*hace falta el scroll???*/
};

struct TileElement{
	string name;
	int pos_x;
	int pos_y;
	int size_x;
	int size_y;
	int value;
	string type;
	bool walkable;
	bool occupied;
};

class Config{
	public:
		Config();
		virtual ~Config();
		Configuration configuration;
		vector<TileElement> elements;
		vector<Tile> tiles;
		string port;
		string ip;
};

#endif