#include <iostream>
#include <fstream>
#include <string>
#include "parser.h"

Parser::Parser(Config *config){
	this->config = config;
	this->logger = Logger::Instance();
};

Parser::~Parser(){
  
};

int Parser::parse(){
	return openFiles();
};

int Parser::openFiles(){
	logger->log(Logger::TypeMessage::LOG_INFO, "Cargando datos");
	string myYaml = "./config_files/map.yaml";

	ifstream file(myYaml);
	int parse_result = 0;
	string line;

	if (file.fail()){
		char msg[200];
		//sprintf_s(msg, "Error reading yaml: %s", strerror(errno));
		logger->log(Logger::TypeMessage::LOG_ERROR, msg);
		return -1;
	} else{
		try{

			nodeConfig = YAML::Load(file);
		}
		catch (YAML::ParserException){
			logger->log(Logger::TypeMessage::LOG_INFO, "Error en el formato del archivo: map.yaml... :(");
			parse_result = -1;
		}

		return Parser::parseConfiguration();
	}
};

int Parser::parseConfiguration(){
	if (nodeConfig["connection"]){
		config->port = nodeConfig["connection"]["port"].as<string>();
	}
	else{
		config->port = "7777";
	}

	if (nodeConfig["configuration"]["height"])
		config->configuration.height = nodeConfig["configuration"]["height"].as<int>();
	else
		config->configuration.height = 800;

	if (nodeConfig["configuration"]["width"])
		config->configuration.width = nodeConfig["configuration"]["width"].as<int>();
	else
		config->configuration.width = 600;

	if (nodeConfig["configuration"]["velocity"])
		config->configuration.velocity = nodeConfig["configuration"]["velocity"].as<int>();
	else
		config->configuration.velocity = 5;

	if (nodeConfig["configuration"]["margin_scroll"])
		config->configuration.scroll = nodeConfig["configuration"]["margin_scroll"].as<int>();
	else
		config->configuration.scroll = 150;

	if (nodeConfig["configuration"]["size_x"])
		config->configuration.sizex = nodeConfig["configuration"]["size_x"].as<int>();
	else
		config->configuration.sizex = 30;

	if (nodeConfig["configuration"]["size_y"])
		config->configuration.sizey = nodeConfig["configuration"]["size_y"].as<int>();
	else
		config->configuration.sizey = 30;

	if (nodeConfig["configuration"]["scrollVelocity"])
		config->configuration.scrollVelocity = nodeConfig["configuration"]["scrollVelocity"].as<int>();
	else
		config->configuration.scrollVelocity = 40;
	
	if(nodeConfig["scenario"]["name"])
		cout << nodeConfig["scenario"]["name"].as<string>();
	
	if(nodeConfig["scenario"]["design"]){
		YAML::Node list = nodeConfig["scenario"]["design"];
		int size = list.size();
		for (int i = 0; i < size; i++) {
			TileElement elem;
			if (list[i]["name"] && list[i]["pos_x"] && list[i]["pos_y"] && list[i]["type"]){
			
				elem.name  = list[i]["name"].as<string>();
				elem.pos_x = list[i]["pos_x"].as<int>();
				elem.pos_y = list[i]["pos_y"].as<int>();
				elem.type  = list[i]["type"].as<string>();
				
				elem.occupied = list[i]["occupied"].as<string>() == "true";
				if(elem.occupied && elem.type != "resource")
					elem.walkable = false;
				else
					elem.walkable = true;

				if (elem.type == "resource")
					elem.value = list[i]["value"].as<int>();
				else
					elem.value = 0;

				if(list[i]["size_x"])
					elem.size_x = list[i]["size_x"].as<int>();
				else
					elem.size_x = 1;

				if(list[i]["size_y"])
					elem.size_y = list[i]["size_y"].as<int>();
				else
					elem.size_y = 1;
				
				config->elements.push_back(elem);
			}
		}
	}

	return 0;
}
