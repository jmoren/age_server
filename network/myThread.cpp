#include "myThread.h"

myThread::myThread(LPTHREAD_START_ROUTINE pThreadFunc, LPVOID pThreadFuncParameter){
	threadFunction = pThreadFunc;
	threadArgument = pThreadFuncParameter;
}

void myThread::execute(){
	
	if ((thread = CreateThread(NULL, 0, threadFunction, threadArgument, 0, &threadId)) == NULL){
		return;
	}
}

myThread::~myThread(){
}


DWORD myThread::getThreadId(){
	return threadId;
}
