#ifndef _TCPSOCKET_H
#define _TCPSOCKET_H

#include <iostream>

#include <winsock2.h>
#include <Windows.h>
#include <ws2tcpip.h>
#include "Data.h"

using namespace std;

class TcpSocket{
public:
	TcpSocket(){};
	TcpSocket(string port);
	~TcpSocket();

	SOCKET mySocket;
	string port;
	string name;
	struct addrinfo *addr;

	// initalize library
	static void initialize();

	// server methods
	int bindSocket();
	int listenForClients();
	TcpSocket* acceptClient(string& client);

	// handle communication
	int recieveMessage(char *buffer);
	int sendMessage(char *buffer, int size);
	int sendMessageTotal(int total, int size);
};

#endif