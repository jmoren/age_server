#ifndef _CONNECTION_H
#define _CONNECTION_H

#include <queue>
#include <iostream>
#include <winsock2.h>
#include <Windows.h>
#include <ws2tcpip.h>

#include "tcpSocket.h"
#include "Data.h"
#include "../game/Game.h"

const int MAX_NUM_CLIENTS = 10;

using namespace std;

class Connection{
public:
	Connection(TcpSocket *socket);
	~Connection();
	TcpSocket *socket;
};

class ClientConnection : public Connection{
public:
	ClientConnection(TcpSocket *mySocket, Game *aGame) : Connection(mySocket){
		this->connected = false;
		this->game = aGame;
		this->wait = 0;
		this->in_lobby = true;
	};

	Game *game;
	string name;
	int wait;
	bool connected;
	bool in_lobby;

	void sendFullData();
	int sendConfiguration(GameStatus message);
	int sendPlayers(GameStatus message);
	int sendElements(GameStatus message);
	int sendRoad(GameStatus message);
	int sendKeepAlive();

	void waitForOptions(string name);

	void startLobby(char * buffer);
};


class ServerConnection: public Connection{
public:
	ServerConnection(TcpSocket *mySocket, Game *aGame) : Connection(mySocket){
		game = aGame;
		totalConnections = 0;
		for (int i = 0; i < MAX_NUM_CLIENTS; i++){
			clientConnections[i] = NULL;
		}
	};

	string name;
	Game *game;
	// for server socket
	ClientConnection * clientConnections[MAX_NUM_CLIENTS];
	ClientConnection *getClient(int index);

	int totalConnections;
	void addConnection(ClientConnection *client);
	void removeConnection(ClientConnection *client);
	void notifyAll(GameUpdate m);
	void startAllPlayers();
};


#endif