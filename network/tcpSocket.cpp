#include "tcpSocket.h"

void TcpSocket::initialize(){
	WSADATA wsaData;

	// initialize socket library
	int result = WSAStartup(MAKEWORD(2, 2), &wsaData);
	if (result != 0) {
		// if fails... exit application
		exit(-1);
	}
}

TcpSocket::TcpSocket(string myPort){
	this->port = myPort;
	this->mySocket = INVALID_SOCKET;

	struct addrinfo hints;

	// set address information
	ZeroMemory(&hints, sizeof(hints));
	hints.ai_family = AF_INET;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_protocol = IPPROTO_TCP;    // TCP connection!!!
	hints.ai_flags = AI_PASSIVE;

	// Resolve the server address and port
	int result = getaddrinfo(NULL, "7777", &hints, &addr);

	// should check for errors
	this->mySocket = socket(addr->ai_family, addr->ai_socktype, addr->ai_protocol);
}

TcpSocket::~TcpSocket(){
	closesocket(this->mySocket);
}

int TcpSocket::bindSocket(){
	if (bind(this->mySocket, addr->ai_addr, (int)addr->ai_addrlen) == -1){
		return -1;
	}
	
	return 0;
}

int TcpSocket::listenForClients(){
	int result;
	if ((result = listen(this->mySocket, SOMAXCONN) ) == -1){
		closesocket(this->mySocket);
		WSACleanup();
		return -1;
	}
	return 0;
}

TcpSocket* TcpSocket::acceptClient(string& clientHost)
{
	SOCKET newSocket = INVALID_SOCKET;

	int len = sizeof(struct sockaddr_in);
	struct sockaddr_in clientAddr;

	if ((newSocket = accept(this->mySocket, (struct sockaddr *)&clientAddr, &len)) == INVALID_SOCKET){
		return NULL;
	}

	TcpSocket* newTcpSocket = new TcpSocket();
	newTcpSocket->mySocket = newSocket;
	return newTcpSocket;
}


int TcpSocket::recieveMessage(char *buffer){
	int result = 0, count = 0;
	int size = sizeof(Message);

	while (count < size){
		result = recv(this->mySocket, &buffer[count], sizeof(Message), 0);
		if (result == 0)
			return 0;
		else if (result == SOCKET_ERROR){
			return SOCKET_ERROR;
		}
		else{
			count += result;
		}
	}

	return count;
};

int TcpSocket::sendMessage(char *buffer, int size){
	int result = 0, count = 0;
	
	while (count < size){
		result = send(this->mySocket, buffer + count, size, 0);
		if (result == 0)
			return 0;
		else if (result == SOCKET_ERROR){
			return SOCKET_ERROR;
		}
		else{
			count += result;
		}
	}

	return count;
}

int TcpSocket::sendMessageTotal(int total, int size){
	int result = 0, count = 0;

	char pchar[sizeof(int)] = { 0 };
	memcpy(pchar, &total, size);

	while (count < size){
		result = send(this->mySocket, pchar, size, 0);
		if (result == 0)
			return 0;
		else if (result == SOCKET_ERROR){
			return SOCKET_ERROR;
		}
		else{
			count += result;
		}
	}

	return count;
}
