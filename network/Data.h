#ifndef _DATA_H
#define _DATA_H

#include <string>
#include <iostream>
#include <vector>

#define MAX_DATA_SIZE 255

enum MSG_TYPE {
	NEW_PLAYER = 1,
	RE_CONNECTION = 2,
	MOVEMENT = 3,
	DISCONNECTION = 4,
	LOST_CONNECTION = 5
};

enum PLAYER_STATUS {
	CONNECTED = 1,
	DISCONNECTED = 0,
	DELETED = 2
};

struct Point{
	int x;
	int y;

	void serialize(char *data){
		memcpy(data, this, sizeof(Point));
	}
};

struct Message{
	char name[32];
	int type;
	int posx;
	int posy;

	void deserialize(char * data) {
		memcpy(this, data, sizeof(Message));
	}

	void serialize(char * data) {
		memcpy(data, this, sizeof(Message));
	}

};

struct PlayerStatus{
	int status;
	char kind[32];
	char name[32];
	bool moving;
	int posx;
	int posy;
	int life;

	int gold;
	int wood;
	int stone;
	int food;
	
	void serialize(char * data) {
		memcpy(data, this, sizeof(PlayerStatus));
	}
};

struct GameElement{
	char name[32];
	int id;
	int posx;
	int posy;
	int value;
	char kind[15];
	bool walkable;
	bool occupied;
	bool deleted;
	void serialize(char * data) {
		memcpy(data, this, sizeof(GameElement));
	}
};

struct RemoteConfig{
	char current_name[32];
	int scroll;
	int scrollVelocity;
	int velocity;
	int size_x;
	int size_y;
	
	void serialize(char *buff){
		memcpy(buff, this, sizeof(RemoteConfig));
	}

};

struct GameStatus{
	char current_name[32];
	RemoteConfig config;
	std::vector<PlayerStatus> players;
	std::vector<GameElement> elements;
	std::vector<int> road;
};

struct GameUpdate{
	int total;
	std::vector<PlayerStatus> data;
};

#endif