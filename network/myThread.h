#ifndef _MYTHREAD_H
#define _MYTHREAD_H

#include <windows.h>

class myThread{

public:

	myThread(LPTHREAD_START_ROUTINE pThreadFunc, LPVOID pThreadFuncParameter = NULL);
	~myThread();

	void execute();
	DWORD getThreadId();

private:
	HANDLE thread;
	DWORD  threadId;
	LPTHREAD_START_ROUTINE threadFunction;
	LPVOID threadArgument;
};

#endif