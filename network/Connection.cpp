#include "Connection.h"

Connection::Connection(TcpSocket *mySocket){
	socket = mySocket;
}

Connection::~Connection(){
	if(socket)
		delete socket;
}

ClientConnection *ServerConnection::getClient(int id){
	return clientConnections[id];
}

int ClientConnection::sendKeepAlive(){
	int ka = 200;
	return this->socket->sendMessageTotal(ka, sizeof(int));
}

void ServerConnection::addConnection(ClientConnection *client){
	if (client){
		this->clientConnections[totalConnections] = client;
		totalConnections++;
	}
}

void ServerConnection::removeConnection(ClientConnection *client){
	if (client){
		this->clientConnections[totalConnections] = nullptr;
		totalConnections--;
	}
}

void ServerConnection::startAllPlayers(){
	GameStatus gs;
	ClientConnection *c;
	for (int i = 0; i < totalConnections; i++){
		c = this->clientConnections[i];
		c->in_lobby = false;
		c->sendFullData();
	}
}

void ServerConnection::notifyAll(GameUpdate message){
	int size_players = (message.total * sizeof(PlayerStatus));
	int sentSize = 0;
	char *buffer = new char[size_players];
	memset(buffer, 0, size_players);

	int offset = 0;

	for (size_t i = 0; i < message.data.size(); i++){
		char bPlayer[sizeof(PlayerStatus)] = { 0 };
		PlayerStatus p = message.data.at(i);
		p.serialize(bPlayer);

		memcpy(buffer+offset, bPlayer, sizeof(PlayerStatus));
		offset += sizeof(PlayerStatus);
	}

	for (int i = 0; i < totalConnections; i++){
		TcpSocket *current = this->clientConnections[i]->socket;
		if (current && this->clientConnections[i]->connected){
			// send size of player_status
			sentSize = current->sendMessageTotal(message.total, sizeof(int));
			if (sentSize > 0)
				current->sendMessage(buffer, size_players);
		}
	}

	/*if(buffer)
		delete buffer;*/
}

void ClientConnection::sendFullData(){
	GameStatus gs = game->getCurrentStatus(this->name);
	this->sendConfiguration(gs);
	this->sendElements(gs);
	this->sendPlayers(gs);
	this->sendRoad(gs);

};
int ClientConnection::sendRoad(GameStatus message){
	int sent = 0;
	int size = 0, roadOffset = 0;
	int rSize = message.road.size() * sizeof(int);
	char rbuffer[sizeof(int)] = { 0 };
	char *buffer = new char[rSize];
	for (size_t i = 0; i < message.road.size(); i++){
		int p = message.road.at(i);
		memcpy(buffer + roadOffset, &p, sizeof(int));
		roadOffset += sizeof(int);
	}

	sent = this->socket->sendMessageTotal(message.road.size(), sizeof(int));
	if (sent > 0 && rSize > 0)
		sent = this->socket->sendMessage(buffer, rSize);

	delete buffer;
	return sent;
}

int ClientConnection::sendElements(GameStatus message){
	int sent = 0;
	int elementOffset = 0;
	int eSize = message.elements.size() * sizeof(GameElement);
	char charGE[sizeof(GameElement)] = { 0 };
	char *bufferElements = new char[eSize];

	for (size_t i = 0; i < message.elements.size(); i++){
		GameElement e = message.elements.at(i);
		e.serialize(charGE);
		memcpy(bufferElements + elementOffset, charGE, sizeof(GameElement));
		elementOffset += sizeof(GameElement);

	}
	sent = this->socket->sendMessageTotal(message.elements.size(), sizeof(message.elements.size()));
	if (sent > 0)
		sent = this->socket->sendMessage(bufferElements, eSize);

	delete bufferElements;
	return sent;
}

int ClientConnection::sendPlayers(GameStatus message){
	int sent = 0;
	int playersOffset = 0;
	int pSize = message.players.size() * sizeof(PlayerStatus);
	char charPS[sizeof(PlayerStatus)] = { 0 };
	char *bufferPlayers = new char[pSize];

	for (size_t i = 0; i < message.players.size(); i++){
		PlayerStatus p = message.players.at(i);
		p.serialize(charPS);
		memcpy(bufferPlayers + playersOffset, charPS, sizeof(PlayerStatus));
		playersOffset += sizeof(PlayerStatus);

	}

	sent = this->socket->sendMessageTotal(message.players.size(), sizeof(int));
	if (sent > 0)
		sent = this->socket->sendMessage(bufferPlayers, pSize);

	delete bufferPlayers;
	return sent;
}

int ClientConnection::sendConfiguration(GameStatus message){
	int sent = 0;
	char *bufferConfig   = new char[sizeof(RemoteConfig)];
	
	message.config.serialize(bufferConfig);

	sent = this->socket->sendMessage(bufferConfig, sizeof(RemoteConfig));

	delete bufferConfig;
	return sent;
}

void ClientConnection::startLobby(char * buffer){
	Player *p;
	Message message;
	int result;
	int intents = 0;

	while (!this->connected && intents < 5){
		message.deserialize(buffer);

		message.posy = game->canPlayMode(message.posx);
		p = game->findPlayer(string(message.name));

		switch (message.posy){
		case 1:
			// puede jugar
			if (p && p->connected == PLAYER_STATUS::CONNECTED){
				message.posy = 4;
			}
			else{
				if (p){
					p->connected = PLAYER_STATUS::CONNECTED;
					message.posy = 1;
				}
				else{
					game->addPlayer(string(message.name));
					p = game->findPlayer(string(message.name));
					p->connected = PLAYER_STATUS::CONNECTED;
				}

				this->in_lobby = false;

			}

			break;
		case 2:
			// tiene que esperar mas jugadores
			if (p && p->connected == PLAYER_STATUS::CONNECTED){
				// ya existe este player
				message.posy = 4;
			}
			else{

				message.posy = 2;
				if (p){
					p->connected = PLAYER_STATUS::CONNECTED;
					message.posy = 1;
				}
				else{
					game->addPlayer(string(message.name));
					p = game->findPlayer(string(message.name));
					p->connected = PLAYER_STATUS::CONNECTED;
				}

				this->in_lobby = true;
			}

			break;
		case 3:
			message.posy = 3;
			break;
		case 4:
			message.posy = 5;
			break;

		}

		message.serialize(buffer);
		result = this->socket->sendMessage(buffer, sizeof(Message));
			
		if (message.posy == 4 || message.posy == 5){
			memset(buffer, 0, sizeof(buffer));
			this->socket->recieveMessage(buffer);
		}
		else{
			this->connected = true;
			this->name = string(message.name);
		}
	}
	
}