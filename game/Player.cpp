#include "Player.h"

Player::Player(){
	this->life = 100;
	this->wait = 0;
	this->is_moving = false;
	this->hasUpdates = true;
	this->mode = 0;

	resources.insert(std::pair<string,int>("gold",0) );
	resources.insert(std::pair<string,int>("wood",0) );
	resources.insert(std::pair<string,int>("stone",0) );
	resources.insert(std::pair<string,int>("food",0) );
}

Player::~Player(){

}

void Player::addResource(string name, int value){
	resources[name] = resources[name] + value;
	this->hasUpdates = true;
}

bool Player::shouldDiscconect(){
	return (this->wait > 10);
}

void Player::setFinalPos(Point point){
	final_pos =  point;
}

bool Player::hasNextStep(){
	return (this->road.size() > 0);
}

void Player::setFinalPos(){
	this->is_moving = false;
	this->hasUpdates = true;
	this->current_pos = this->final_pos;
}

void Player::moveNext(){
	this->is_moving = true;
	this->hasUpdates = true;

	Point p = this->road.back();
	this->current_pos.x = p.x;
	this->current_pos.y = p.y;
	this->road.pop_back();

}