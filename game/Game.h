#ifndef _GAME_H
#define _GAME_H

#include <iostream>
#include <queue>

#include "Player.h"
#include "../network/Data.h"
#include "../parser/config.h"

enum MODE{
	CENTRO_CIVICO = 1,
	ROBAR_BANDERA = 2,
	CAPTURAR_REY = 3
};

class Game{

public:
	Game(Config *config);
	Config *config;

	void updateStatus();
	void movePlayer();
	void collectResource();
	void initialize();

	Tile * getTile(int x, int y);
	int mode;
	bool started;
	void processEvents(queue<Message> *list);
	void disconnect(Player *player);
	void processMessage(Message message);
	void movePlayer(string name, int posx, int posy); 
	bool checkLobby();

	Point assignInitalPos();
	int assignId();

	GameUpdate getUpdates();
	GameStatus getCurrentStatus(string name);
	void incrementWaitForAll();
	void addDiscoveredRoad(Player*p, Tile*t);
	void addPlayer(string name);
	void disconnectPlayer(Message m);
	void calculateRoadFor(Message m);
	void deleteFromElementList(int posX, int posY);

	void checkForResourcesFor(Player* player);
	vector<Tile*> tiles;
	map<int, Element*> elements;
	map<string, Player*> players;
	GameUpdate updates;
	char *prepareData();
	int globalID;
	Player *findPlayer(string name);
	int sizex;
	int sizey;
	bool hasUpdatesToSend;
	
	int canPlayMode(int mode);
private:
	void Game::cleanUpdates();
};

#endif