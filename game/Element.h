#pragma once
#include <iostream>

class Element
{
public:
	Element(std::string name, int posX, int posY);
	~Element(void);

	int id;
	std::string name;
	std::string kind;
	int posx;
	int posy;
	int value;
	bool walkable;
	bool occupied;
	bool deleted;
};

