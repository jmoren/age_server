#ifndef TILE_HEADER
#define TILE_HEADER

#include "Player.h"
#include "Element.h"

class Tile {
public:
	Tile(int tileX, int tileY){
		posx = tileX;
		posy = tileY;
		// Tile constructor
		this->elem = nullptr;
		this->occupied = false;
		this->walkable = true;
		this->visited = false;
	};

	~Tile();
	
	bool is_walkable();
	Element* getElem();

	void linkElement(Element *elem);
	void unlinkElement();
	bool visited;
	int posx;
	int posy;
	bool occupied;
	bool walkable;

private:
	Element *elem;

};

#endif

