#include "Tile.h"



Tile::~Tile(){
	this->elem = nullptr;
	this->occupied = false;
	this->walkable = false;
	this->visited = false;
}


bool Tile::is_walkable(){
	return this->walkable;
}

void Tile::linkElement(Element *newElem){
	this->elem = newElem;
}

void Tile::unlinkElement(){
	this->elem = nullptr;
}

Element* Tile::getElem(){
		return elem;
}