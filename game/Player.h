#ifndef PLAYER_HEADER
#define PLAYER_HEADER

#include <map>
#include <iostream>
#include <string>

#include "../network/Data.h"
#include <vector>

using namespace std;

class Player{
public:
	Player();
	~Player();

	string name;
	int life;
	int connected;
	int mode;
	map<string, int> resources;
	Point current_pos;

	bool is_moving;
	bool hasUpdates;
	Point final_pos;
	Point prox_pos;
	map<int, bool>discovered;
	int wait;
	bool shouldDiscconect();
	bool hasNextStep();
	void moveNext();
	void setFinalPos();
	vector<Point> road;
	void setFinalPos(Point point);
	
	void addResource(string name, int value);
};

#endif