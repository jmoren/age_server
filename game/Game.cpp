#include "Game.h"
#include "Tile.h"
#include "Element.h"

#include "../AStarAlgorithm/AStar.h"

Game::Game(Config *aConfig){
	this->config = aConfig;
	this->initialize();
	this->hasUpdatesToSend = false;
	this->started = false;
	this->mode = 1;
}

Tile * Game::getTile(int x, int y){
	return this->tiles.at(y * config->configuration.sizey + x);
}

void Game::initialize(){
	int element_id = 0;
	
	//load plains tiles
	this->sizex = this->config->configuration.sizex;
	this->sizey = this->config->configuration.sizey;
	
	for (int y = 0; y < sizey; y++){
		for (int x = 0; x < sizex; x++){
			Tile *t = new Tile(x, y);
			
			this->tiles.push_back(t);

		}
	}

	//load elements
	Tile *mapTile;

	for (size_t i = 0; i < config->elements.size(); i++){
		element_id++;
		TileElement element = config->elements[i];
		int posX = element.pos_x;
		int posY = element.pos_y;
		int size_x = element.size_x;
		int size_y = element.size_y;
		int val = element.value;
		string name = element.name;

		mapTile = this->getTile(posX, posY);
		Element *entity  = new Element(name, posX, posY);
		entity->id       = element_id;
		entity->value    = element.value;
		entity->kind     = element.type;
		entity->walkable = element.walkable;

		entity->occupied = element.occupied;

		// set the entity linked to the tile

		mapTile->linkElement(entity);	
		mapTile->walkable = entity->walkable;
		mapTile->occupied = entity->occupied;


		for(int i=0; i < element.size_x && !entity->walkable; i++){
			for(int j=0; j < element.size_y && !entity->walkable; j++){
					mapTile = this->getTile(posX + i, posY - j);
					mapTile->occupied = true;
					mapTile->linkElement(entity);
					mapTile->walkable = entity->walkable;
					mapTile->occupied = entity->occupied;
					//mapTile->getElem()->walkable = false;
					//cout << mapTile->posx << "," << mapTile->posy << " Ocupada" << endl;
				}
			}

		this->elements.insert(make_pair(entity->id, entity));
	}

}

Point Game::assignInitalPos(){
	Point p;
	bool valid = false;

	while (!valid){		
		int random = rand();	
		p.x = random % this->sizex;
		p.y = random % this->sizey;

		Tile * t = this->getTile(p.x, p.y);
		
		valid = t->is_walkable();
	}
	
	return p;
}

/****** Messages ******** 
/*  Is a new player
**		type: 1
**		name: "MyName"
**		player_id: -1
**		posx: 0
**		posy: 0
/*  Is a re-connection
**		type: 2
**		name: "MyName"
**		player_id: my_id
**		posx: my_posx
**		posy: my_posy
/*  Is a mmovement
**		type: 3
**		name: "MyName"
**		player_id: my_id
**		posx: new_posx
**		posy: new_posy
/*  Is a disconnection
**		type: 4
**		name: "MyName"
**		player_id: my_id
**		posx: 0
**		posy: 0
/** Genereated by the client - lost connection
**		type: 5
**		name: ""
**		player_id: client->id
**		posx: client->posx
**		posy: client->posy
*/

void Game::processMessage(Message message){
	Player * p;

	Message m = message;
	int message_type = m.type;

	switch (message_type){
	case 1:
		this->addPlayer(m.name);
		break;
	case 3:
		p = this->players[m.name];
		this->movePlayer(p->name, m.posx, m.posy);
		break;
	case 4:
		this->disconnectPlayer(m);
		break;
	default:
		break;
	}
};


void Game::processEvents(queue<Message> *list){
	Message m;

	while ((*list).size() > 0){
		m = (*list).front();
		this->processMessage(m);
		(*list).pop();
	}
}


void Game::disconnectPlayer(Message m){
	Player * p = this->players[m.name];
	if (p){
		p->connected = PLAYER_STATUS::DISCONNECTED;
		p->is_moving = false;
		p->hasUpdates = true;
	}
}

void Game::addPlayer(string newName){
	// search if player exists before create new one
	Player *player = this->findPlayer(newName);

	if (player){
		player->connected = PLAYER_STATUS::CONNECTED;
		player->hasUpdates = true;
		player->is_moving = false;

		Tile *t = this->getTile(player->current_pos.x, player->current_pos.y);
		t->occupied = true;
		t->walkable = false;
	}
	else{
		Point pos = this->assignInitalPos();

		player = new Player();
		player->name = string(newName);
		player->current_pos = pos;
		player->prox_pos = pos;
		player->final_pos = pos;
		player->connected = PLAYER_STATUS::CONNECTED;
		Tile *t = this->getTile(pos.x, pos.y);
		t->occupied = true;
		t->walkable = false;
		this->players[player->name] = player;

	}

	// mark the tile as occupied
	Tile *t = this->tiles.at(player->current_pos.y * this->sizey + player->current_pos.x);
	t->occupied = true;
	t->walkable = false;

}

void Game::movePlayer(string myName, int posx, int posy){
	Player * player = this->players[myName];
	vector<Position *> proad;

	if (player->current_pos.x != posx || player->current_pos.y != posy){
		player->final_pos.x = posx;
		player->final_pos.y = posy;

		AStar *aStar = new AStar(posx, posy, player->current_pos.x, player->current_pos.y, this->sizex, this->sizey, this->tiles);
		proad = aStar->calculateRoad();

		while (!proad.empty()){
			Position* pos = proad.front();
			Point npoint;
			npoint.x = pos->getX();
			npoint.y = pos->getY();
			player->road.push_back(npoint);
			proad.erase(proad.begin());
		}
	
		player->hasUpdates = true;
		player->is_moving = true;

		delete aStar;
	}
}

int Game::canPlayMode(int aMode){
	int max = 5;
	int total = 0;
	map<string, Player*>::iterator p;
	Player *aPlayer = nullptr;
	if (this->mode == aMode){
		total = this->players.size();
	
		if (total > 0 && total < max && this->started){
			// can play
			return 1;
		}
		else if (total >= 0 && total < max && !this->started){
			// wait
			return 2;
		}
		else{
			// mode full
			return 3;
		}
	}
	else{
		return 4;
	}
}

Player * Game::findPlayer(string newName){
	map<string, Player*>::iterator p;
	Player *aPlayer = nullptr;
	for (p = players.begin(); p != players.end(); p++) {
		if (p->second && p->second->name.compare(newName) == 0){
			aPlayer = p->second;
		}
	}

	return aPlayer;
}

void Game::checkForResourcesFor(Player* player){
	Tile *tile = this->tiles.at(player->current_pos.y * this->sizey + player->current_pos.x);
	Element *e = tile->getElem();
	if(e && e->kind == "resource"){
		player->addResource(e->name, e->value);
		e->value = 0;
		tile->occupied = false;
		tile->unlinkElement();
	}		
}

// We return the message to send all clients
GameUpdate Game::getUpdates(){
	GameUpdate gu;
	map<string, Player*>::iterator p;
	map<int, Element*>::iterator e;

	this->hasUpdatesToSend = false;
	gu.total = 0;

	for (p = players.begin(); p != players.end(); p++) {
		Player *player = p->second;
		PlayerStatus ps;

		if (player){
			if (player->connected == PLAYER_STATUS::CONNECTED){
				if (player->hasNextStep()){
					Point p = player->road.back();

					Tile *tileMov = this->tiles.at(player->current_pos.y * this->sizey + player->current_pos.x);

					tileMov->occupied = false;
					tileMov->walkable = true;

					if (!this->getTile(p.x, p.y)->is_walkable())
						this->movePlayer(player->name, player->final_pos.x, player->final_pos.y);

					player->moveNext();
					this->checkForResourcesFor(player);

					tileMov = this->tiles.at(player->current_pos.y * this->sizey + player->current_pos.x);
					tileMov->occupied = true;
					tileMov->walkable = false;
					this->addDiscoveredRoad(player, tileMov);
				}
				else{
					if (player->is_moving)
						player->setFinalPos();
				}

				if (player->hasUpdates){
					ps.status = PLAYER_STATUS::CONNECTED;
					ps.posx = player->current_pos.x;
					ps.posy = player->current_pos.y;
					ps.moving = player->is_moving;
					ps.life = player->life;
					ps.food = player->resources["food"];
					ps.gold = player->resources["gold"];
					ps.wood = player->resources["wood"];
					ps.stone = player->resources["stone"];

					strncpy_s(ps.name, player->name.c_str(), sizeof(ps.name));
					strncpy_s(ps.kind, "player", sizeof(ps.kind));

					gu.data.push_back(ps);
					gu.total += 1;

					this->hasUpdatesToSend = true;

					if (!player->is_moving)
						player->hasUpdates = false;
				}
			}
			else {
				if (player->hasUpdates){

					ps.status = PLAYER_STATUS::DISCONNECTED;
					ps.posx = player->current_pos.x;
					ps.posy = player->current_pos.y;
					ps.moving = player->is_moving;
					ps.life = player->life;
					ps.food = player->resources["food"];
					ps.gold = player->resources["gold"];
					ps.wood = player->resources["wood"];
					ps.stone = player->resources["stone"];

					strncpy_s(ps.name, player->name.c_str(), sizeof(ps.name));
					strncpy_s(ps.kind, "player", sizeof(ps.kind));

					gu.data.push_back(ps);
					gu.total += 1;

					// the first time, we sent a message
					if (!player->is_moving)
						player->hasUpdates = false;

					this->hasUpdatesToSend = true;
				}
			}
		}
	}
	
	for (e = this->elements.begin(); e != this->elements.end(); e++) {
		Element *elem = e->second;
		if (elem){
			if (elem->kind.compare("resource") == 0 && elem->value == 0 && !elem->deleted){
				PlayerStatus ps;
				ps.posx = elem->posx;
				ps.posy = elem->posy;
				ps.life = elem->value;
				ps.status = PLAYER_STATUS::DELETED;
				ps.food = 0;
				ps.stone = 0;
				ps.wood = 0;
				ps.gold = 0;
				strncpy_s(ps.name, elem->name.c_str(), sizeof(ps.name));
				strncpy_s(ps.kind, "resource", sizeof(ps.kind));

				gu.data.push_back(ps);
				gu.total += 1;

				this->hasUpdatesToSend = true;
				elem->deleted = true;
			}

		}
	}

	return gu;
}

void Game::incrementWaitForAll(){
	map<string, Player*>::iterator p;
	for (p = this->players.begin(); p != this->players.end(); p++) {
		Player *aPlayer = p->second;
		if (aPlayer && aPlayer->connected){
			aPlayer->wait += 1;
		}
	}
};

// for each new player we parse the game and put all inside a message to send him
GameStatus Game::getCurrentStatus(string name){
	GameStatus gm;
	map<string, Player*>::iterator p;
	map<int, Element*>::iterator e;
	map<int, bool>::iterator r;

	strncpy_s(gm.current_name, name.c_str(), sizeof(gm.config.current_name));
	strncpy_s(gm.config.current_name, name.c_str(), sizeof(gm.config.current_name));
	gm.config.scroll = this->config->configuration.scroll;
	gm.config.scrollVelocity = this->config->configuration.scrollVelocity;
	gm.config.size_x = this->config->configuration.sizex;
	gm.config.size_y = this->config->configuration.sizey;
	gm.config.velocity = this->config->configuration.velocity;

	// if player already exists
	Player *player = this->findPlayer(name);
	if (player){
		for (r = player->discovered.begin(); r != player->discovered.end(); r++){
			int pos = r->first;
			gm.road.push_back(pos);
		}
	}

	for (p = this->players.begin(); p != this->players.end(); p++) {
		Player *aPlayer = p->second;
		if (aPlayer){
			PlayerStatus ps;

			ps.posx = aPlayer->current_pos.x;
			ps.posy = aPlayer->current_pos.y;
			ps.life = aPlayer->life;
			ps.gold = aPlayer->resources["gold"];
			ps.wood = aPlayer->resources["wood"];
			ps.stone = aPlayer->resources["stone"];
			ps.food = aPlayer->resources["food"];
			ps.moving = aPlayer->is_moving;
			ps.status = aPlayer->connected;
			
			strncpy_s(ps.name, aPlayer->name.c_str(), sizeof(ps.name));
			strncpy_s(ps.kind, "player", sizeof(ps.kind));
			gm.players.push_back(ps);
		}
	}
	for (e = this->elements.begin(); e != this->elements.end(); e++) {
		Element *elem = e->second;
		GameElement ge;
		strncpy_s(ge.name, elem->name.c_str(), sizeof(ge.name));
		strncpy_s(ge.kind, elem->kind.c_str(), sizeof(ge.kind));
		ge.walkable = elem->walkable;
		ge.occupied = elem->occupied;
		ge.posx = elem->posx;
		ge.posy = elem->posy;
		ge.value = elem->value;
		ge.id = elem->id;
		ge.deleted = elem->deleted;
		gm.elements.push_back(ge);
	}
	return gm;
}

void Game::addDiscoveredRoad(Player *p, Tile *t){
	int pos = t->posy * this->config->configuration.sizey + t->posx;
	p->discovered[pos] = true;
}

bool Game::checkLobby(){
	int total = 0;
	map<string, Player*>::iterator p;
	for (p = this->players.begin(); p != this->players.end(); p++) {
		Player *aPlayer = p->second;
		if (aPlayer && aPlayer->connected)
			total += 1;
	}

	return (total > 1);
}