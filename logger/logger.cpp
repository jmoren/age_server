#include "logger.h"


const char *TypeLogString[] = { " [INFO] ", " [WARNING] ", " [ERROR] " };

Logger* Logger::m_pInstance = NULL;

Logger* Logger::Instance(){
	if (!m_pInstance)
		m_pInstance = new Logger();
	return m_pInstance;
}

Logger::~Logger(){
	fstream file(filename, ios_base::app);
	if (file.is_open()){
		char message[100];
		sprintf_s(message, "Errores: %d, Warnings: %d", errors, warnings);
		log(Logger::TypeMessage::LOG_INFO, "---------------------");
		log(Logger::TypeMessage::LOG_INFO, message);
		file.close();
	}

	m_pInstance = nullptr;
}

const char * Logger::getLogTypeString(TypeMessage type){
	return TypeLogString[type];
}

void Logger::log(Logger::TypeMessage type, int value, string message){
	fstream file(filename, ios_base::app);
	char formatted[180];
	sprintf_s(formatted, message.c_str(), value);
	if (file.is_open()){
		file << getCurrentTime() << getLogTypeString(type) << formatted << endl;
		Logger::registerEvents(type);
	}
}

void Logger::log(TypeMessage type, string message){
	fstream file(filename, ios_base::app);
	if (file.is_open()){
		file << getCurrentTime() << getLogTypeString(type) << message << endl;
		Logger::registerEvents(type);
	}
}

void Logger::registerEvents(TypeMessage type){
	switch (type) {
	case TypeMessage::LOG_ERROR:
		++errors;
		break;
	case TypeMessage::LOG_WARNING:
		++warnings;
		break;
	}
}
string Logger::getCurrentTime(){
	char buf[80];
	time_t now = time(0);
	struct tm timeinfo;
	localtime_s(&timeinfo, &now);
	strftime(buf, sizeof(buf), "%Y-%m-%d %I:%M%p", &timeinfo);
	return buf;
}