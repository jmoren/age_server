#ifndef   _LOGGER_H_
#define   _LOGGER_H_

#include <fstream>
#include <string>
#include <time.h>

using namespace std;

class Logger{
public:
	static enum TypeMessage { LOG_INFO, LOG_WARNING, LOG_ERROR };
	static Logger* Instance();
	void log(TypeMessage type, string message);
	void log(TypeMessage type, int value, string message);

	void registerEvents(TypeMessage type);
	~Logger();
private:
	fstream file;
	string filename;
	int errors;
	int warnings;

protected:
	Logger(){
		filename = "./log_file.log";
		errors = 0;
		warnings = 0;
		fstream file(filename, fstream::out);
		if (file.is_open()){
			file << getCurrentTime() + " Age of Empires 2015 - Server - Open LOGFILE" << endl;
		}
	};

	Logger(Logger const&){};
	Logger& operator=(Logger const&){};
	static Logger* m_pInstance;

	string getCurrentTime();
	const char * getLogTypeString(TypeMessage type);
};

#endif